package util;

import java.util.Arrays;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class IntList {
	private int[] intList;
	
	public IntList() {
		this.intList = new int[0];
	}
	
	public IntList(int[] intList){
		this.intList = new int[intList.length];
		for (int i = 0; i < intList.length; i++) {
			this.intList[i] = intList[i];
		}
	}
	
	public int size() { //param�terrel nem lenne egyszer�bb??
		return intList.length;
	}
	
	public int get() {
		return intList[0];
	}
	
	public int get(int i) {
		return intList[i];
	}
	
	public void add(int numberToAdd) {
		int[] list = Arrays.copyOf(intList, intList.length+1);
		list[intList.length] = numberToAdd;
		intList = Arrays.copyOf(list, list.length);
	}
	
	public void add(int numberToAdd, int i) {
		int[] tmp = new int[intList.length+1];
		for (int j = 0; j < i; j++) {
			tmp[j] = intList[j];
		}
		tmp[i] = numberToAdd;
		for (int j = i+1; j < intList.length+1; j++) {
			tmp[j] = intList[j-1];
		}
		intList = tmp;
	}
	
	public String toString() {
		String list = "";
		for (int i = 0; i < intList.length; i++) {
			list +=  intList[i];
			if(i != intList.length-1) {
				list += ", ";
			}
		}
		return list;
	}
	
	public static IntList fromString(String text) {
		String[] splitted = text.split(" ");
		int[] list = new int[splitted.length];
		for (int i = 0; i < splitted.length; i++) {
			try {
				list[i] = Integer.parseInt(splitted[i]);
			} catch (Exception e) {
				return null;
			}
		}
		return new IntList(list); 
	}
	
	public void set(int i, int x){
	    if(i < intList.length){
	      intList[i] = x;
	    }
	 }
	
	public void remove() {
		int[] list = Arrays.copyOf(intList, intList.length-1);
		intList = Arrays.copyOf(list, list.length);
	}
	
	public int remove(int i) {
		int[] tmpList = new int[intList.length-1];
		for (int j = 0; j < i; j++) {
			tmpList[i] = intList[i];
		}
		int number = intList[i];
		
		for (int j = i; j < tmpList.length; j++) {
			tmpList[i] = intList[i+1];
		}
		intList = tmpList;
		
		return number;
	}
	
	public int indexOf(int j){
	    for(int i = 0; i< intList.length; i++){
	      if(intList[i] == j ){
	        return i;
	      }
	    }
	    return -1;
	  }
	
	public void concat(IntList tmp) {
		int[] list = new int[tmp.size() + intList.length];
		for (int i = 0; i < intList.length; i++) {
			list[i] = intList[i];
		}
		for (int i = 0; i < tmp.size(); i++) {
			list[intList.length + i] = tmp.get(i);
		}
		intList = list;
	}
	
	public int[] getIntList() {
		return intList;
	}

	public void setIntList(int[] intList) {
		this.intList = intList;
	}

}
