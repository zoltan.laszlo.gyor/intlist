package java_exercises4;

import util.IntList;

public class Main {
	public static void main(String[] args) {
		IntList list = new IntList();
		System.out.println(list.size());
		list.add(11);
		list.add(33);
		list.add(55);
		list.add(77);
		list.add(99, 0);
		System.out.println(list.get());
		
		list.set(0, 999);
		System.out.println(list.get());
		
		list.remove();
		list.remove(0);
		System.out.println(list.get());
		System.out.println(list.indexOf(11));
		
		System.out.println(list.toString());
		
		IntList tmpList = IntList.fromString("3 4 5 6 7 1");
		System.out.println(tmpList.toString());
		
		
		
		list.concat(tmpList);
		System.out.println(list.toString());
	}	
}
